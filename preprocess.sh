#!/bin/bash

# SPDX-FileCopyrightText: 2024 the-dream-tim
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

set -e

# preprocess.sh: this script should also be included for reproducibility.
# It setups computation that usually run only one time, such as creating a document database.

ROOT=$(dirname "$0")
cd "$ROOT"

module purge
module load cpuarch/amd
module load pytorch-gpu/py3/2.1.1

python main.py preprocess "$@"