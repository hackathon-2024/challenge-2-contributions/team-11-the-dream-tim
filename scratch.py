# SPDX-FileCopyrightText: 2024 the-dream-tim
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# %%
import os
from pathlib import Path
import pickle
import typer
import pandas as pd
import re

import numpy as np
import torch
from torch import Tensor
from sentence_transformers import SentenceTransformer

from main import BLOB_FILE, compute_similiarities

DEBUG = "./zay/debug.pkl"

df = pickle.load(open(DEBUG, "rb"))

df

# %%
blob = pickle.load(BLOB_FILE.open("rb"))
similiarities = compute_similiarities(df, blob)

# %%
import plotly.express as px

px.histogram(similiarities[3])

# %%
px.scatter(similiarities.max(axis=1).values)