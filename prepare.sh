#!/bin/bash

# SPDX-FileCopyrightText: 2024 the-dream-tim
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# run.sh: (mandatory). It will be called with a path to the questions csv file and with a path to an output directory.
# It should set-up your environment, such as Jean-Zay module loading and python environment sourcing, then run your program.
# Your program should read each entry of the csv, then output the corresponding the corresponding answer, the prompt used to generate such answer and url corresponding to the document used to generate the answer.