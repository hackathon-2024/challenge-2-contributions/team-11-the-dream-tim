<!--
SPDX-FileCopyrightText: 2024 the-dream-tim
SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: EUPL-1.2
-->

# Team Name : The Dream Tim

## Licence / License

:fr: Ce code a été produit pour le Challenge 2 du Hackathon 2024 co-organisé par le PEReN et la Commission Européenne : DSA RAG Race. Il est délivré comme récupéré à la fin de l'événement et peut donc ne pas être exécutable.

Sauf mention contraire, le code source est placé sous licence publique de l'Union Européenne, version 1.2 (EUPL-1.2).

Les données des plateformes présentes sur ce projet sont la propriété des plateformes et ne sont fournies qu'à titre d'illustration pour le bon fonctionnement du projet. Elles ne seront en aucun cas mises à jour. Les données complètes peuvent être trouvées vers ce lien : https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions


:gb: This code was developed for Challenge 2 of the Hackathon 2024 co-organized by the PEReN and the European Commission: DSA RAG Race. It is released as retrieved at the end of the event and may therefore not be executable.

Unless otherwise specified, the source code is licensed under the European Union Public License, version 1.2 (EUPL-1.2).

The data of platforms contained in this project are the property of the platforms and are provided for illustrative purposes only. They will not be updated under any circumstances. The complete data can be found at the following link: https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions


## Librairies utilisées

torch  
plotly  
pandas  
sentence-transformers  
vllm  
typer  

cf `pyproject.toml`

## Exécuter le code

```bash
bash run.sh
```