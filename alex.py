# SPDX-FileCopyrightText: 2024 the-dream-tim
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

template = """

You can find above a piece of text that can be short (e.g. a question) or long (e.g. a paragraph from a website).
Your goal is to write a short English sentence that contain all the keywords from the text and concisely describe its content. Focus on the general concepts, not the examples given.

Example 1:

---
File: 'Facebook_API-Ad'

Ads that were delivered to the European Union will also have:

An example query and responseTo retrieve data on ads about social issues, elections or politics that contain the term "**california**" and that reached an audience in the United States, you could enter this query:

`curl -G \`

`-d "search_terms='california'" \`

`-d "ad_type=POLITICAL_AND_ISSUE_ADS" \`

`-d "ad_reached_countries=['US']" \`

`-d "access_token=<ACCESS_TOKEN>" \`

`"https://graph.facebook.com/<API_VERSION>/ads_archive"`
----

Short summary: "EU ads: impressions, demographics, financials; U.S. political ad queries for 'California'."

Example 2:

---
Écris-moi une requête curl. Cette requête doit me permettre d'obtenir, auprès de Facebook, les publicités politiques contenant le mot "europe" et qui ont atteint la France et la Belgique. La réponse ne doit contenir que le code de la requête curl.
---

Short summary: "Curl request for Facebook political ads with keywords."

Example 3:

---
File: https://about.linkedin.com/transparency/government-requests-report
|     | Requests for member data | Accounts subject to request \[1\] | Percentage LinkedIn provided data | Accounts LinkedIn provided data \[2\] |
| Argentina | 2   | 2   | 0%  | 0   |
| Australia | 2   | 3   | 50% | 1   |
| Austria | 6   | 7   | 0%  | 0   |
| Bangladesh | 1   | 1   | 0%  | 0   |
| Belgium | 4   | 5   | 100% | 4   |
---

Short summary: "LinkedIn government requests report: Country-wise data requests and compliance, Jan-Jun 2023."

Example 4:

---
{TEXT}
---

Short summary: \""""


def get_key_from_text(texts: list[str], big: bool = False) -> list[str]:
    """If the text is a chunk, the file name should be prefixed using 'File:' """

    prompts = [template.format(TEXT=text) for text in texts]
    outputs = run_model(prompts, big=big)
    clean_outputs = []
    for out_text in outputs:
        clean_outputs.append(out_text.partition('"')[0])
    return clean_outputs


from functools import cache
from vllm import LLM, SamplingParams
from vllm import LLM
from vllm.model_executor.parallel_utils.parallel_state import destroy_model_parallel


@cache
def get_model(big: bool = True):
    destroy_model_parallel()
    if big:
        return LLM(model="meta-llama/Llama-2-13b-chat-hf",
            dtype="float16") #"/gpfsdswork/dataset/HuggingFace_Models/EleutherAI/gpt-neo-125M")
    else:
        return LLM(model="meta-llama/Llama-2-7b-chat-hf",
            dtype="float16") #"/gpfsdswork/dataset/HuggingFace_Models/EleutherAI/gpt-neo-125M")

prompt_template = """
You are a helpful, respectful and honest assistant.
You always relies on information available in your context to generate your answers.<</SYS>>

{QUESTION}"""


def run_model(prompt_list, max_tok=300, big=True):
    llm = get_model(big)

    outputs = []
    sampling_params = SamplingParams(temperature=0., top_p=0.95, max_tokens=max_tok)
    prompt_text = [prompt_template.format(QUESTION=prompt) for prompt in prompt_list]
    outputs = llm.generate(prompt_text, sampling_params)
    generated_texts = []
    for output in outputs:
        prompt = output.prompt
        generated_text = output.outputs[0].text
        generated_texts.append(generated_text)
    return generated_texts


Template = """

I need to answer questions unsing information contained from an excerpt from a documentation webpage. The answer is in the language in which the question is asked.

## Example 1:

---- Context ----
Url: https://developers.facebook.com/docs/graph-api/reference/ads_archive/

In the case of estimated values, aggregated demographic data is based on a number of factors, including age and gender information users provide in their Facebook profile.
An example query and responseTo retrieve data on ads about social issues, elections or politics that contain the term "california" and that reached an audience in the United States, you could enter this query:
curl -G \
-d "search_terms='california'" \
-d "ad_type=POLITICAL_AND_ISSUE_ADS" \
-d "ad_reached_countries=['US']" \
-d "access_token=<ACCESS_TOKEN>" \
"https://graph.facebook.com/<API_VERSION>/ads_archive"
Which would return a response like this:

---------

Question: Écris-moi une requête curl. Cette requête doit me permettre d'obtenir, auprès de Facebook, les publicités politiques contenant le mot "europe" et qui ont atteint la France et la Belgique. La réponse ne doit contenir que le code de la requête curl.

Answer: ```curl -G -d "search_terms=\'europe\'" -d "ad_type=POLITICAL_AND_ISSUE_ADS" -d "ad_reached_countries=[\'FR\',\'BE\']" -d "access_token=<ACCESS_TOKEN>" "https://graph.facebook.com/<API_VERSION>/ads_archive"```

## Example 2:

---- Context ----
Url: https://policy.pinterest.com/en/digital-services-act-transparency-report

**Team**

Our team consists of specialists trained in Pinterest’s Guidelines and specialists trained to review content against laws of the EU and its member states. In the event of complex reports, team members can escalate any questions to leads of our Trust & Safety Operations team, who may in turn consult with specialists within and outside the company trained in laws of the EU and its member states.

Every member of our review team speaks at least one language of a member state of the EU, and we have external resources available to assist with any language not listed in the table below. See below for a detailed breakdown of how many review team members speak the following EU member state official languages:

| Language | Internal specialists | External specialists |
| --- | --- | --- |
| Danish | 0   | 1   |
| English | 45  | 338 |
| French | 1   | 5   |
| German | 2   | 5   |
| Italian | 0   | 2   |
| Latvian | 1   | 0   |
| Portuguese | 4   | 1   |
| Spanish | 7   | 5   |
| Swedish | 1   | 0   |
---------

Question: How many languages spoken in the EU are spoken by the Pinterest team ? Can you specify the link where this information is mentioned ?

Answer: There are 9 languages from the EU spoken by the Pinterest team. These languages are the EU member state official languages : Danish, English, French, German Italian, Latvian, Portuguese, Spanish, Swedish. You can find these information at this link : https://policy.pinterest.com/en/digital-services-act-transparency-report

## Example 3:

---- Context ----
Url: https://learn.microsoft.com/en-us/advertising/guides/ad-library-api?view=bingads-13

### Authenticating your credentials

Ad Library API uses the same authentication schemes as Bing Ads API. For details about authenticating Microsoft account credentials with OAuth, see [Authentication with the Microsoft identity platform](https://learn.microsoft.com/en-us/advertising/guides/authentication-oauth-identity-platform).

You can use the Bing Ads SDK for .NET, Java, or Python to authenticate Microsoft account credentials. For details about using the SDK to get the access token, see [C#](https://learn.microsoft.com/en-us/advertising/guides/get-started-csharp) | [Java](https://learn.microsoft.com/en-us/advertising/guides/get-started-java) | [Python](https://learn.microsoft.com/en-us/advertising/guides/get-started-python).

_Note_: The Bing Ads SDK doesn't provide interfaces for Ad Library API. You should only use the SDK to get the access token if you're using the SDK for Microsoft Advertising campaigns, too. Otherwise, it may not be worth the overhead of installing the SDK.
---------

Question: Comment savoir si une campagne publicitaire sur Facebook a dévié de sa cible prévue ? La réponse doit contenir un lien vers la documentation des API de Facebook au sujet des publicités. La réponse doit nommer les champs les plus pertinents disponibles via cette API.

Answer: Le contexte donné correspond campagnes publicitaires sur Bing et non Facebook. Je n'ai pas accès aux sources pour répondre à la question demandée.

## Example 4:

---- Context ----
Url: https://values.snap.com/fr-FR/privacy/transparency/european-union

| Slovaquie | 592,399 |
| Slovénie | 444,642 |
| Espagne | 4,499,952 |
| Suède | 4,574,610 |
| TOTAL | 101,973,520 |

Ces chiffres servent à répondre aux règles actuelles du DSA et ne doivent être utilisés qu’à des fins de DSA. Nous pouvons modifier la manière dont nous évaluons ce chiffre au fil du temps, y compris en réponse à l’évolution des recommandations des régulateurs et de la technologie. Cela peut également différer des estimations utilisées pour d’autres chiffres d’utilisateur actif que nous publions à d’autres fins.

Représentant légal

Snap Group Limited a désigné Snap B.V. comme son représentant légal aux fins du DSA. Vous pouvez contacter le représentant sur dsa-enquiries \[at\] snapchat.com, via notre Site d’assistance \[[ici](https://help.snapchat.com/hc/requests/new?start=5749439348080640%3Futm_source%3Dweb&utm_medium=snap&utm_campaign=transparency&lang=fr-FR)\], ou à :

Snap B.V.
Keizersgracht 165, 1016 DP
Amsterdam, Pays-Bas

Si vous êtes un organisme chargé de l’application de la loi, veuillez suivre les étapes décrites [ici](https://values.snap.com/fr-FR/en-GB/safety/safety-enforcement).
---------

Question: How can I contact Snapchat DSA legal assistant?

Answer: You can contact Snap B.V. at these addresses: dsa-enquiries [at] snapchat.com or Snap B.V.<br/>Keizersgracht 165, 1016 DP<br/>Amsterdam, Netherlands.

## Example 5:

---- Context ----
{CONTEXT}
---------

Question: {QUESTION}
Answer:"""


def generate_answers(chuncks: list[str], questions: list[str]) -> tuple[list[str], list[str]]:
    """chuncks - list[str] avec Url headers !!
    questions - list[str] """
    prompts = [Template.format(CONTEXT = CONTEXT, QUESTION=QUESTION) for (CONTEXT,QUESTION) in zip(chuncks,questions)]
    outputs = run_model(prompts, big=True)
    outputs = [
        output.partition("## Example")[0]
        for output in outputs
    ]
    return outputs, prompts